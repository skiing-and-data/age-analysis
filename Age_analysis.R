# FIS point and age analysis

# Load required packages -----------------------------------------------------
library (tidyverse)
library (lubridate)
library (lme4)
library (lmerTest)
library (plotly)

# Read in Points data --------------------------------------------------------
source ("Read_FIS.R")

# Data Cleaning and Calculations ---------------------------------------------

# Clean data for our purposes
# Remove skiers with no points or points over 1000
fis <- fis [is.na (fis$DIpoints)==FALSE,]
fis <- fis [fis$DIpoints < 1000,]
# Remove skiers with NA gender and select skiers identifying as Male or Female
fis <- fis [fis$Gender %in% c("M", "W"),]
fis$Gender <- factor (fis$Gender, levels=c("M", "W"), labels=c("Men", "Women"))

# Calculate exact age at time of point calculation
fis$Birthdate <- ymd (fis$Birthdate)
fis$Calculationdate <- ymd (fis$Calculationdate)
fis$Age <- (fis$Birthdate %--% fis$Calculationdate) / years(1)

# Remove skiers less than 16 years old
# These are likely mistakes
fis <- fis [fis$Age>16,]

# Drop skiers with fewer than 5 FIS races in the last 12 months
# And other weird codes
things <- c ("4", "3", "2", "1", "<", "+", ".")
sum (fis$DISta %in% things)
fis <- fis [!(fis$DISta %in% things),]

# create age squared variable for later modeling
fis$Age2 <- fis$Age^2

# Create summary data frame with age, points, number of seasons, etc for each skier
fis.ind <- fis %>% 
  group_by (Competitorid, Gender, Competitorname, Nationcode) %>%
  summarize(minAge=min (Age, na.rm=TRUE),
            maxAge=max (Age, na.rm=TRUE),
            minPoints=min (DIpoints),
            maxPoints=max (DIpoints),
            seasons=n())
fis.ind <- fis.ind [-nrow (fis.ind),]
fis.ind <- fis.ind [order (-fis.ind$seasons),]


# Plotting all skiers  -------------------------------------------------------

# Subset data based on lowest FIS points to create performance tiers
comp.10 <- fis.ind$Competitorid [fis.ind$minPoints < 10]
comp.50 <- fis.ind$Competitorid [fis.ind$minPoints < 50]
comp.100 <- fis.ind$Competitorid [fis.ind$minPoints < 100]
comp.200 <- fis.ind$Competitorid [fis.ind$minPoints < 200]
comp.500 <- fis.ind$Competitorid [fis.ind$minPoints < 500]

# Plot of FIS points vs age for all skiers
ggplot (fis, aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="#006069")

# Plot of FIS points vs age for skiers who have had points under 10
p10 <- ggplot (data=fis [fis$Competitorid %in% comp.10,], aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="006069") +
  ylim (0, 200)

# Plot of FIS points vs age for skiers who have had points under 50
p50 <- ggplot (data=fis [fis$Competitorid %in% comp.50,], aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="#006069") +
  ylim (0, 200)

# Plot of FIS points vs age for skiers who have had points under 100
p100 <- ggplot (data=fis [fis$Competitorid %in% comp.100,], aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="#0D47A1") +
  ylim (0, 500) 
# Plot of FIS points vs age for skiers who have had points under 200
p200 <- ggplot (data=fis [fis$Competitorid %in% comp.200,], aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="#673AB7") 

# Plot of FIS points vs age for skiers who have had points under 500
p500 <- ggplot (data=fis [fis$Competitorid %in% comp.500,], aes (x=Age, y=DIpoints)) +
  geom_point (alpha=0.1) +
  theme_bw () +
  facet_wrap(~Gender) +
  geom_smooth(color="#880E4F") 


# pre/post 2010 Comparison ---------------------------------------------------

# Why do we have u-shaped trend compared to Joran's downward trend?
# Have things changed since 2010?
fis$Era <- ifelse (fis$Year<2010, "Before", "Since")

ggplot (data=fis [fis$Competitorid %in% comp.50,],
        aes (x=Age, y=DIpoints, color=Era, group=Era)) +
  geom_point (alpha=0.05) +
  scale_color_manual(values=c("#26C6DA", "#880E4F"),
                     name="Time \nPeriod",
                     labels=c("Before \n2010", "Since \n2010")) +
  ylab ("FIS Distance Points") +
  theme_minimal () +
  facet_wrap(~Gender) +
  geom_smooth() +
  ylim (0, 200)


# Analysis of 20 season skiers -----------------------------------------------

# Subset only skiers that have held a FIS license for 20 + years
fis.ind.20 <- fis.ind [fis.ind$seasons>=20,]
fis20 <- fis [fis$Competitorid %in% fis.ind.20$Competitorid,]

# These are very much outliers in the data
#pdf ("Season_histogram.pdf", height=3, width=4)
ggplot (fis.ind, aes (x=seasons)) +
  geom_histogram (breaks=0:28) +
  theme_classic() +
  labs (x="Number of FIS Seasons", y="Number of Skiers")
#dev.off ()

# We looked up each of these 40 athletes and found the date of their last WC or OWG race
fis.ind.20.old <- read.csv ("Athletes20Seasons.csv")
fis.ind.20 <- merge (fis.ind.20, fis.ind.20.old[,c(1,9)], by= "Competitorid", all.x = TRUE)
fis20 <- merge (fis20, fis.ind.20)
fis20$Retired <- ifelse (fis20$Year>fis20$lastWC, "Yes", "No")


# Fit a quadratic mixed effects model to calculate age of minimum points for 20 season skiers
summary (mod20m <- lmer (DIpoints ~ Age + Age2 + (1|Competitorid),
                         data=fis20[fis20$Gender=="Men",]))
summary (mod20w <- lmer (DIpoints ~ Age + Age2 + (1|Competitorid),
                         data=fis20[fis20$Gender=="Women",]))
# Calculate minimum age
# When you asked in highschool when you would ever use algebra and calculus, this is the answer
(peakAgeMen2 <- -(mod20m@beta [2]/(2*mod20m@beta[3])))
(peakAgeWomen2 <- -(mod20w@beta [2]/(2*mod20w@beta[3])))

# Create a data frame to graph the line
Age <- seq (from=15, to=45, by=0.1)
eq.m <- mod20m@beta [1] + mod20m@beta [2]*Age + mod20m@beta [3]*Age^2
eq.w <- mod20w@beta [1] + mod20w@beta [2]*Age + mod20w@beta [3]*Age^2
mod.df <- data.frame(Age=rep(Age, 2), predicted=c(eq.m, eq.w),
                     Gender= c(rep("Men", length(Age)), rep("Women", length(Age))))


# Plot 20 season skiers

# Plot points vs age with quadratic line
plot20season1 <- ggplot (data=fis20, aes (x=Age, y=DIpoints)) +
  geom_line (mapping=aes (group=Competitorname, color=Competitorname), alpha=0.3) +
  geom_line (data=mod.df, aes (y=predicted), size=1.5, color="006069") +
  theme_minimal () +
  facet_wrap(~Gender) +
  ylab ("FIS Distance Points") 


# Plot points vs age with geom smooth line
plot20season2 <- ggplot (data=fis20, aes (x=Age, y=DIpoints)) +
  geom_line (mapping=aes (group=Competitorname), color="#696969", alpha=0.5) +
  theme_minimal () +
  facet_wrap(~Gender) +
  geom_smooth(color="#0DBCD4") +
  ylab ("FIS Distance Points")

#pdf ("agePlot20Seasons1.pdf", height=3, width = 5)
plot20season2
#dev.off()

# Plot points vs age with separate geom smooth lines before and after WC retirement
plot20season3<- ggplot (data=fis20, aes (x=Age, y=DIpoints)) +
  geom_line (mapping=aes (group=Competitorname, color=Retired), alpha=0.5) +
  scale_color_manual(values=c("#880E4F", "#0DBCD4"),
                     name="",
                     labels=c("Before Last \nWorld Cup Start",
                              "After Last \nWorld Cup Start")) +
  theme_minimal () +
  facet_wrap(~Gender) +
  geom_smooth(color="#0DBCD4") +
  geom_smooth(data=fis20 [fis20$Retired=="No",], color="#880E4F") +
  ylab ("FIS Distance Points")

#pdf ("agePlot20Seasons2.pdf", height=3, width = 6)
plot20season3
#dev.off()

# Check out the situation before 2010
plot20seasons2010 <- ggplot (data=fis20 [fis20$Year<2010,], aes (x=Age, y=DIpoints)) +
  #geom_point (alpha=0.1) +
  geom_line (mapping=aes (group=Competitorname), alpha=0.3) +
  theme_minimal () +
  facet_wrap(~Gender) +
  geom_smooth(color="006069") +
  ylab ("FIS Distance Points") +
  ylim (0, 200)


# Basic demographic stats ----------------------------------------------------
# Gender breakdown
table (fis.ind.20$Gender) #42.5% female
table (fis.ind$Gender) # 37.2% female

# age break down
n <- 40
sum (fis$Age>n & fis$Year<2010, na.rm=TRUE)
sum (fis$Age>n & fis$Year>=2010, na.rm=TRUE)
sum (fis$Age>n, na.rm=TRUE)

sum (fis$Age>n & fis$Year<2010, na.rm=TRUE)/sum (fis$Year<2010, na.rm=TRUE)
sum (fis$Age>n & fis$Year>=2010, na.rm=TRUE)/sum (fis$Year>=2010, na.rm=TRUE)

